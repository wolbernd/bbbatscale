[![Quality Gate Status](https://bbb-scale.sonar.fbi.h-da.de/api/project_badges/measure?project=bbbatscale&metric=alert_status)](https://bbb-scale.sonar.fbi.h-da.de/dashboard?id=bbbatscale)
[![Maintainability Rating](https://bbb-scale.sonar.fbi.h-da.de/api/project_badges/measure?project=bbbatscale&metric=sqale_rating)](https://bbb-scale.sonar.fbi.h-da.de/dashboard?id=bbbatscale)
[![Reliability Rating](https://bbb-scale.sonar.fbi.h-da.de/api/project_badges/measure?project=bbbatscale&metric=reliability_rating)](https://bbb-scale.sonar.fbi.h-da.de/dashboard?id=bbbatscale)
[![Security Rating](https://bbb-scale.sonar.fbi.h-da.de/api/project_badges/measure?project=bbbatscale&metric=security_rating)](https://bbb-scale.sonar.fbi.h-da.de/dashboard?id=bbbatscale)
[![Coverage](https://bbb-scale.sonar.fbi.h-da.de/api/project_badges/measure?project=bbbatscale&metric=coverage)](https://bbb-scale.sonar.fbi.h-da.de/dashboard?id=bbbatscale)

# BBB@Scale

BBB@Scale aims to be an all-in-one portal for managing tenants, users, rooms, recordings, and BBB servers in order to provide a solution for large-scale online learning scenarios.

Some of the features include:

- Manage rooms (meetings) and their configuration in one spot.
- Manage BBB servers (cloud, on-premises, mixed) to provide a scale-out infrastructure. Just add new servers.
- Manage tenants, users and roles (we can distinguish teachers and students based on LDAP groups)
- Manage BBB servers in the cloud or on-premises. All you need is a BBB server with its shared secret.
- Manage room types for different settings (e.g. lecture rooms can only be started by moderators, all participants start muted, no guests are allowed).
- Basic stats and monitoring. See the current utilization of the servers grouped by tenants.
- Import/Export the server and rooms configuration for backup or transfer to a different portal.
- Disable servers on the portal for maintenance or when there is a server acting strangely and you want to remove it from the cluster quickly.
- Responsive UI

Ready to go? Lets [get started](GettingStarted.md)!

## Project Status

**We have officially released 1.0.0!**

This means that essential features for operating a stable environment have been implemented and we are ready to share it with the world.

For the upcoming weeks after release, we are focusing our effort to work on:

- Better documentation
- Better customization
- Moodle integration
- Improved unit tests

Join the newsletter to get the latest updates on the project: [Join Newsletter](https://seu2.cleverreach.com/f/256102-252386/) / [Leave Newsletter](https://seu2.cleverreach.com/f/256102-252386/wwu/)

If you have questions which are not answered by this ReadMe or [Getting Started Guide ](GettingStarted.md) feel free to write us a mail at mail@bbbatscale.de([PGP-Key](mailAtBBBatScale.de_public.pgp)).

If you happen to know a language in which you want to use BBBatScale, you already wrote some documentation for yourself, or have anything else that is worth sharing, contributions are always welcome.

You will be able to find more information about that shortly under [Contributing.md](Contributing.md).

## Background

„BigBlueButton is a web conferencing system designed for online learning“ and it’s a perfect fit for a variety of use cases. During the SARS-CoV-2 outbreak of 2020, we (the Department of Computer Science at University of Applied Sciences Darmstadt) faced the challenging task of transferring all of our lectures to the digital space in no time. After evaluating some online learning and web conference tools, we decided that BBB best suits our needs.

Our first pragmatic solution was to create a virtual room for every physical room at the faculty. Having virtual rooms represent physical rooms made it very easy to integrate the systems into the campus management system.

Each virtual room was hosted on its own BBB virtual machine. Additionally, we created a small web portal based on Django to provide a single point of entry for students and teachers as well as to decouple the mapping between rooms and servers. The solution survived some load testing and the upcoming semester was saved, but we wasted resources. Furthermore, using this approach for the entire university meant to use a tremendous amount of VMs. More than we actually had!

So we thought about better scaling and placement of rooms on BBB servers and found [Scalelite](https://github.com/blindsidenetworks/scalelite). Scalelite is a load balancer for BBB and our first tests showed that it worked just fine. However, we decided not to add additional components to our architecture in order to keep things as simple as possible. Instead, we improved and extended our web portal to fit our needs of managing all required entities in order to provide a solution for large-scale online learning scenarios.

## Screenshots

### Architectural overview

![Arcitecture](/Documentation/architecture.png?raw=true 'Architectural overview')

### Home screen. Listing all available rooms for users and guests

![Home](/Documentation/Screenshots/1.home.png?raw=true 'Home')

### Statistics screen. Showing the current utilization of the servers grouped by tenants

![Statistics](/Documentation/Screenshots/2.stats.png?raw=true 'Statistics')

### Admin rooms screen. Managing rooms...

![Rooms](/Documentation/Screenshots/3.rooms.png?raw=true 'Rooms')

### Admin server screen. Managing servers...

![Server](/Documentation/Screenshots/4.server.png?raw=true 'Server')

### Admin room types screen. Managing room types aka room default configurations

![Room Types](/Documentation/Screenshots/5.roomtypes.png?raw=true 'Room Types')

### Start a room. While starting a room, a user can customize the room's default configuration

![Start Room](/Documentation/Screenshots/6.roomstart.png?raw=true 'Start a room')
