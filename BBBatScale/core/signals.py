from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
import uuid
from django.utils.crypto import get_random_string
from core.models import Room
import logging

logger = logging.getLogger(__name__)


@receiver(post_save, sender=Room)
def set_values_after_creation(instance, created, *args, **kwargs):
    if created:
        if not instance.meeting_id:
            instance.meeting_id = uuid.uuid4().hex
        instance.attendee_pw = get_random_string(length=10)
        instance.moderator_pw = get_random_string(length=10)
        instance.save()
