# encoding: utf-8
import logging
from datetime import timedelta
from django.core.management.base import BaseCommand
from django.utils.timezone import now
from core.models import Room
from core.services import reset_room

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'perform health check on all instances'

    def handle(self, *args, **options):
        logger.info("Start housekeeping")
        # clear monitoring values on inactive rooms
        for room in Room.objects.filter(last_running__lte=now() - timedelta(seconds=120)):
            if room.is_breakout:
                room.delete()
            else:
                reset_room(room.meeting_id, room.name, room.config)
        logger.info("End housekeeping")
