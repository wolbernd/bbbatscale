# Generated by Django 3.0.4 on 2020-04-01 22:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0030_auto_20200331_1201'),
    ]

    operations = [
        migrations.RenameField(
            model_name='instance',
            old_name='shared_sected',
            new_name='shared_secret',
        ),
        migrations.RemoveField(
            model_name='instance',
            name='conferences',
        ),
        migrations.RemoveField(
            model_name='instance',
            name='cpu_usage',
        ),
        migrations.RemoveField(
            model_name='instance',
            name='largest_conference',
        ),
        migrations.RemoveField(
            model_name='instance',
            name='network_usage',
        ),
        migrations.RemoveField(
            model_name='instance',
            name='participants',
        ),
        migrations.RemoveField(
            model_name='instance',
            name='rtt_aggregate',
        ),
        migrations.RemoveField(
            model_name='instance',
            name='videostreams',
        ),
        migrations.AlterField(
            model_name='instance',
            name='dns',
            field=models.CharField(max_length=255, unique=True),
        ),
        migrations.AlterField(
            model_name='room',
            name='type',
            field=models.CharField(choices=[('LECTURE', 'Vorlesung'), ('LAB', 'Labor'), ('CONFERENCE', 'Konferenz')], default='CONFERENCE', max_length=255),
        ),
    ]
