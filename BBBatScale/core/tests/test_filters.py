from django.test import TestCase

from core.models import Room, Tenant, Instance, RoomConfiguration
from core.constants import INSTANCE_STATE_UP, INSTANCE_STATE_DISABLED
from core.filters import RoomFilter, TenantFilter, InstanceFilter
from django.http import QueryDict


class CoreFilterTestCase(TestCase):

    def setUp(self) -> None:
        self.fbi = Tenant.objects.create(name="FBI")
        self.hda = Tenant.objects.create(name="HDA")
        self.room_config1 = RoomConfiguration.objects.create(name="Config1")
        self.room_config2 = RoomConfiguration.objects.create(name="Config2")
        self.bbb1_instance = Instance.objects.create(
            dns="bbb1.fbi.h-da.de",
            datacenter="fbi",
            tenant=self.fbi,
            state=INSTANCE_STATE_UP
        )
        self.bbb2_instance = Instance.objects.create(
            dns="bbb2.fbi.h-da.de",
            datacenter="fbi1337",
            tenant=self.fbi,
            state=INSTANCE_STATE_DISABLED
        )
        self.bbb3_instance = Instance.objects.create(
            dns="bbb3.h-da.de",
            datacenter="hda",
            tenant=self.hda,
            state=INSTANCE_STATE_UP
        )
        self.room1 = Room.objects.create(
            name="D14/00.01",
            tenant=self.fbi,
            config=self.room_config1
        )
        self.room2 = Room.objects.create(
            name="D15/00.01",
            tenant=self.fbi,
            config=self.room_config2
        )
        self.room3 = Room.objects.create(
            name="Mathlab",
            tenant=self.hda,
            config=self.room_config2,
            instance=self.bbb2_instance
        )

    def test_room_filter(self):
        get_params = QueryDict('', mutable=True)
        get_params.update({'name': '/'})
        f = RoomFilter(get_params, Room.objects.all())
        self.assertQuerysetEqual(f.qs, Room.objects.filter(name__icontains="/"), transform=lambda x: x)
        get_params.pop('name')
        get_params.update({'tenant': self.fbi.pk})
        f = RoomFilter(get_params, Room.objects.all())
        self.assertQuerysetEqual(f.qs, Room.objects.filter(tenant=self.fbi.pk), transform=lambda x: x)
        get_params.pop('tenant')
        get_params.update({'instance': self.bbb2_instance.pk})
        f = RoomFilter(get_params, Room.objects.all())
        self.assertQuerysetEqual(f.qs, Room.objects.filter(instance=self.bbb2_instance.pk), transform=lambda x: x)
        get_params.pop('instance')
        get_params.update({'config': self.room_config2.pk})
        f = RoomFilter(get_params, Room.objects.all())
        self.assertQuerysetEqual(f.qs, Room.objects.filter(config=self.room_config2.pk), transform=lambda x: x)

    def test_instance_filter(self):
        get_params = QueryDict('', mutable=True)
        get_params.update({'dns': 'fbi'})
        f = InstanceFilter(get_params, Instance.objects.all())
        self.assertQuerysetEqual(f.qs, Instance.objects.filter(dns__icontains="fbi"), transform=lambda x: x)
        get_params.pop('dns')
        get_params.update({'tenant': str(self.hda.pk)})
        f = InstanceFilter(get_params, Instance.objects.all())
        self.assertQuerysetEqual(f.qs, Instance.objects.filter(tenant=self.hda.pk), transform=lambda x: x)
        get_params.pop('tenant')
        get_params.update({'dns': 'fbi1337'})
        f = InstanceFilter(get_params, Instance.objects.all())
        self.assertQuerysetEqual(f.qs, Instance.objects.filter(datacenter__icontains='fbi1337'), transform=lambda x: x)
        get_params.pop('dns')
        get_params.update({'state': INSTANCE_STATE_UP})
        f = InstanceFilter(get_params, Instance.objects.all())
        self.assertQuerysetEqual(f.qs, Instance.objects.filter(state=INSTANCE_STATE_UP), transform=lambda x: x)

    def test_tenant_filter(self):
        get_params = QueryDict('', mutable=True)
        get_params.update({'name': 'HDA'})
        f = TenantFilter(get_params, Tenant.objects.all())
        self.assertQuerysetEqual(f.qs, Tenant.objects.filter(name__icontains="HDA"), transform=lambda x: x)
